package com.panderalabs.pact.feignconsumer

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslJsonArray
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.RequestResponsePact
import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import feign.Feign
import feign.FeignException
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import feign.okhttp.OkHttpClient
import feign.slf4j.Slf4jLogger
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith


const val PROVIDER_NAME = "BookService"
const val CONSUMER_NAME = "FeignClient"
const val PORT = "8089"

/**
 * JUnit 5 Test
 */
@ExtendWith(PactConsumerTestExt::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FeignBookClientPactTest {


    private enum class ProviderState(val desc: String) {
        ZERO_BOOKS("Zero Books Exist"),
        TWO_BOOKS("Two Books Exist"),
        EFFECTIVE_JAVA("Book 0134685997 exists")
    }

    private lateinit var client: FeignBookClient

    private val effectiveJavaBook = Book("Effective Java (3rd Edition)", "Joshua Bloch", "0134685997")
    private val updatedEffectiveJavaBook = Book("Effective Java (3rd Edition)", "Josh Bloch", "0134685997")

    private val bookMatchingBody = newJsonBody { o ->
        o.stringMatcher("isbn", "\\d{10}|\\d{13}", updatedEffectiveJavaBook.isbn)
        o.stringType("title", effectiveJavaBook.title)
        o.stringType("author", effectiveJavaBook.author)
    }.build()!!

    private val effectiveJavaBody = newJsonBody { o ->
        o.stringValue("isbn", updatedEffectiveJavaBook.isbn)
        o.stringValue("title", effectiveJavaBook.title)
        o.stringValue("author", effectiveJavaBook.author)
    }.build()!!

    private val effectiveJavaUpdatedBody = newJsonBody { o ->
        o.stringValue("isbn", updatedEffectiveJavaBook.isbn)
        o.stringValue("title", updatedEffectiveJavaBook.title)
        o.stringValue("author", updatedEffectiveJavaBook.author)
    }.build()!!

    @BeforeAll
    fun setup() {
        val modules: List<Module> = listOf(ParameterNamesModule())

        client = Feign.builder()
            .logger(Slf4jLogger())
            .client(OkHttpClient())
            .encoder(JacksonEncoder(modules))
            .decoder(JacksonDecoder(modules))
            .target(FeignBookClient::class.java, "http://localhost:$PORT/books")
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun defaultBookListPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.TWO_BOOKS.desc)
            .uponReceiving("get list of all books")
            .path("/books")
            .method("GET")
            .headers(mapOf("Accept" to "application/json"))
            .willRespondWith()
            .headers(mapOf("Content-Type" to "application/json"))
            .status(200)
            .body(
                PactDslJsonArray.arrayMinLike(1, 2)
                    .stringMatcher("isbn", "\\d{10}|\\d{13}", effectiveJavaBook.isbn)
                    .stringType("title", effectiveJavaBook.title)
                    .stringType("author", effectiveJavaBook.author)
                    .closeObject()
            )
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun getBookPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.EFFECTIVE_JAVA.desc)
            .uponReceiving("get book by isbn")
            .path("/books/0134685997")
            .method("GET")
            .headers(mapOf("Accept" to "application/json"))
            .willRespondWith()
            .headers(mapOf("Content-Type" to "application/json"))
            .status(200)
            .body(bookMatchingBody)
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun createBookPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.ZERO_BOOKS.desc)
            .uponReceiving("post new book")
            .path("/books")
            .method("POST")
            .headers(mapOf("Accept" to "application/json"))
            .headers(mapOf("Content-Type" to "application/json"))
            .body(effectiveJavaBody)
            .willRespondWith()
            .status(201)
            // Add location header
            .matchHeader("Location", "http.*", "http://localhost:8080/books/0134685997")
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun deleteBookPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.EFFECTIVE_JAVA.desc)
            .uponReceiving("delete book by isbn")
            .path("/books/0134685997")
            .method("DELETE")
            .willRespondWith()
            .status(204)
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun updateBookPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.EFFECTIVE_JAVA.desc)

            .uponReceiving("put for book")
            .path("/books/0134685997")
            .method("PUT")
            .headers(mapOf("Accept" to "application/json"))
            .headers(mapOf("Content-Type" to "application/json"))
            .body(effectiveJavaUpdatedBody)
            .willRespondWith()
            .status(200)
            //Assume exact response as was provided
            .body(effectiveJavaUpdatedBody)
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun nonExistentBookPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .uponReceiving("get for non-existent book")
            .path("/books/11111111111111")
            .method("GET")
            .headers(mapOf("Accept" to "application/json"))
            .willRespondWith()
            .status(404)
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun invalidISBNPact(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .uponReceiving("post with invalid ISBN")
            .path("/books")
            .method("POST")
            .headers(mapOf("Accept" to "application/json"))
            .headers(mapOf("Content-Type" to "application/json"))
            .body(newJsonBody { o ->
                o.stringValue("isbn", "000000000")
                o.stringValue("title", "Fake Book")
                o.stringValue("author", "Fake Author")
            }.build())
            .willRespondWith()
            .status(400)
            .toPact()
    }

    @Test
    @PactTestFor(pactMethod = "defaultBookListPact", port = PORT)
    fun testGetBooks(server: MockServer): Unit {
        val books = client.books
        assertTrue(books.isNotEmpty())
    }

    @Test
    @PactTestFor(pactMethod = "nonExistentBookPact", port = PORT)
    fun testGetNonExistentBook(server: MockServer): Unit {
        val exception = assertThrows(FeignException::class.java) {
            val book = client.getBook("11111111111111")
        }
        assertEquals(404, exception.status())
    }

    @Test
    @PactTestFor(pactMethod = "invalidISBNPact", port = PORT)
    fun testCreateBookWithInvalidISBN(server: MockServer): Unit {
        val exception = assertThrows(FeignException::class.java) {
            client.createBook(Book("Fake Book", "Fake Author", "000000000"))
        }
        assertEquals(400, exception.status())
    }

    @Test
    @PactTestFor(pactMethod = "createBookPact", port = PORT)
    fun testCreateBook(server: MockServer): Unit {
        client.createBook(effectiveJavaBook)
    }

    @Test
    @PactTestFor(pactMethod = "deleteBookPact", port = PORT)
    fun testDeleteBook(server: MockServer): Unit {
        client.deleteBook(effectiveJavaBook.isbn)
    }

    @Test
    @PactTestFor(pactMethod = "getBookPact", port = PORT)
    fun testGetBook(server: MockServer): Unit {
        val book = client.getBook(effectiveJavaBook.isbn)
        assertEquals(effectiveJavaBook, book)
    }

    @Test
    @PactTestFor(pactMethod = "updateBookPact", port = PORT)
    fun testUpdateBook(server: MockServer): Unit {
        val updatedBook = client.updateBook(updatedEffectiveJavaBook.isbn, updatedEffectiveJavaBook)
        assertEquals(updatedEffectiveJavaBook, updatedBook)
    }
}
