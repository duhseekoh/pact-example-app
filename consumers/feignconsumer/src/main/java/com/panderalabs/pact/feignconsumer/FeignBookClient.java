package com.panderalabs.pact.feignconsumer;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;

@Headers("Accept: application/json")

public interface FeignBookClient {

    @RequestLine("GET ")
    @NotNull
    Collection<Book> getBooks();

    @RequestLine("POST ")
    @Headers("Content-type: application/json")
    void createBook(Book book);

    @RequestLine("GET /{isbn}")
    Book getBook(@Param("isbn") String isbn);

    @RequestLine("PUT /{isbn}")
    @Headers("Content-type: application/json")
    Book updateBook(@Param("isbn") String isbn, Book book);

    @RequestLine("DELETE /{isbn}")
    void deleteBook(@Param("isbn") String isbn);
}
