package com.panderalabs.pact.feignconsumer;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class FeignConsumer {

    public static void main(String[] args) {

        FeignBookClient client =
                Feign.builder()
                        .encoder(new JacksonEncoder())
                        .decoder(new JacksonDecoder())
                        .target(FeignBookClient.class, "https://localhost:8080/books");

        System.out.println(client.getBooks());
    }
}
