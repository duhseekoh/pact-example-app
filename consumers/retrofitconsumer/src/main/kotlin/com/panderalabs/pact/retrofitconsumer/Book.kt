package con.panderalabs.pact.retrofitconsumer

import java.time.Year

data class Book(var title: String, var author: String, var isbn: String, var year: Year?)
