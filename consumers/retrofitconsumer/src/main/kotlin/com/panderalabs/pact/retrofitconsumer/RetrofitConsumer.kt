package con.panderalabs.pact.retrofitconsumer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import com.panderalabs.pact.retrofitconsumer.RetrofitBookClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


fun main(args: Array<String>) {
    val mapper = ObjectMapper()
    mapper.registerModules(ParameterNamesModule(), KotlinModule(), JavaTimeModule())

    val retrofit = Retrofit.Builder()
        .baseUrl("http://localhost:8080/")
        .addConverterFactory(JacksonConverterFactory.create(mapper))
        .build()

    val client: RetrofitBookClient = retrofit.create(RetrofitBookClient::class.java)

    println(client.getBooks().execute().body().toString())
}
