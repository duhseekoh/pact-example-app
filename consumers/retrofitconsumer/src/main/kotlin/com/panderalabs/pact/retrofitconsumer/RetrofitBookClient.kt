package com.panderalabs.pact.retrofitconsumer


import con.panderalabs.pact.retrofitconsumer.Book
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitBookClient {

    @GET("/books")
    fun getBooks(): Call<Collection<Book>>

    @GET("/books/{isbn}")
    fun getBook(@Path("isbn") isbn: String): Call<Book>
}
