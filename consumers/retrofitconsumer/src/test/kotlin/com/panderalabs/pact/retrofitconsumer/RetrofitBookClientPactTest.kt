package com.panderalabs.pact.retrofitconsumer

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PM
import au.com.dius.pact.consumer.dsl.PactDslJsonArray
import au.com.dius.pact.consumer.dsl.PactDslJsonBody
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.RequestResponsePact
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import con.panderalabs.pact.retrofitconsumer.Book
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.time.Year


const val PROVIDER_NAME = "BookService"
const val CONSUMER_NAME = "RetrofitClient"
const val PORT = "8089"

/**
 * JUnit 5 Test
 */
@ExtendWith(PactConsumerTestExt::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RetrofitBookClientPactTest {

    private enum class ProviderState(val desc: String) {
        ZERO_BOOKS("Zero Books Exist"),
        TWO_BOOKS("Two Books Exist"),
        EFFECTIVE_JAVA("Book 0134685997 exists")
    }

    private lateinit var client: RetrofitBookClient


    private val phoenixProjectBook =
        Book("The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win", "Gene Kim", "1942788290", Year.of(2013))


    private val bookMatchingBody =
        PactDslJsonBody()
            .stringMatcher("isbn", "\\d{10}|\\d{13}", phoenixProjectBook.isbn)
            .stringType("title", phoenixProjectBook.title)
            .stringType("author", phoenixProjectBook.author)
            //.stringMatcher("year", "\\d{4}", "2013")
            //.or("year", null, PM.nullValue())
            .or("year", "2013", PM.stringMatcher("\\d{4}"), PM.nullValue())

    @BeforeAll
    fun setup() {
        val mapper = ObjectMapper()
        mapper.registerModules(ParameterNamesModule(), KotlinModule(), JavaTimeModule())

        val retrofit = Retrofit.Builder()
            .baseUrl("http://localhost:$PORT/")
            .addConverterFactory(JacksonConverterFactory.create(mapper))
            .build()

        client = retrofit.create(RetrofitBookClient::class.java)
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun allBooks(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.TWO_BOOKS.desc)
            .uponReceiving("request for all books")
            .path("/books")
            .method("GET")
            .willRespondWith()
            .headers(mapOf("Content-Type" to "application/json"))
            .status(200)
            .body(
                PactDslJsonArray.arrayMinLike(1, 2)
                    .stringMatcher("isbn", "\\d{10}|\\d{13}", phoenixProjectBook.isbn)
                    .stringType("title", phoenixProjectBook.title)
                    .stringType("author", phoenixProjectBook.author)
                    .or("year", "2013", PM.stringMatcher("\\d{4}"), PM.nullValue())
                    .closeObject()
            )
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun oneBook(builder: PactDslWithProvider): RequestResponsePact {
        return builder.given(ProviderState.EFFECTIVE_JAVA.desc)
            .uponReceiving("request book by isbn")
            .path("/books/1942788290")
            .method("GET")
            .willRespondWith()
            .headers(mapOf("Content-Type" to "application/json"))
            .status(200)
            .body(bookMatchingBody)
            .toPact()
    }

    @Pact(provider = PROVIDER_NAME, consumer = CONSUMER_NAME)
    fun invalidBook(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .uponReceiving("request for invalid book")
            .path("/books/0000000000")
            .method("GET")
            .willRespondWith()
            .status(404)
            .toPact()
    }

    @Test
    @PactTestFor(pactMethod = "allBooks", port = PORT)
    fun testGetBooks(server: MockServer): Unit {
        val response = client.getBooks().execute()
        val books = response.body()
        assertTrue(response.isSuccessful)
        assertEquals(200, response.code())
        assertTrue(books != null && books.isNotEmpty())
    }

    @Test
    @PactTestFor(pactMethod = "invalidBook", port = PORT)
    fun testGetNonExistentBook(server: MockServer): Unit {
        val response = client.getBook("0000000000").execute()
        assertFalse(response.isSuccessful)
        assertEquals(404, response.code())
    }

    @Test
    @PactTestFor(pactMethod = "oneBook", port = PORT)
    fun testGetBook(server: MockServer): Unit {
        val response = client.getBook(phoenixProjectBook.isbn).execute()
        assertTrue(response.isSuccessful)
        assertEquals(200, response.code())
    }
}
