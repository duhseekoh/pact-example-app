# PACT Example App


* `providers/spring-boot-provider`
  * Provider for a simple CRUD service for Books implemented as a Spring Boot App
* `consumers/feignconsumer`
  * Read/Write consumer implemented using Feign
* `consumers/retrofitconsumer`
  * Read-only consumer implemented with retrofit


### Test Consumers & Publish Pact Files to broker
`./gradlew test :consumer:retrofitconsumer:pactPublish :consumer:feignconsumer:pactPublish`

### Test Provider against pacts from broker

#### Via pact gradle plugin (launches instance of app, and tests against it)  
`./gradlew :providers:springboot-provider:pactVerify`

#### By Running unit tests
`./gradlew :providers:springboot-provider:test`

### Launch Spring boot app 
`./gradlew :providers:springboot-provider:bootRun`


### Consumer Pact test classes
* `com.panderalabs.pact.retrofitconsumer.RetrofitBookClientPactTest`
* `com.panderalabs.pact.feignconsumer.FeignBookClientPactTest`  

### Provider Pact test classes
* `com.panderalabs.pact.bookserviceprovider.BookServicePactVerificationIntegrationTest`
  * Uses Spring `MockMvc` to test against controllers directly
* `com.panderalabs.pact.bookserviceprovider.BookServicePactVerificationUnitTest`
  * Uses Spring Junit Runner to launch app server and tests against it


## Broker
Pact broker: https://panderalabs.pact.dius.com.au
Credentials are in 1password...

