package com.panderalabs.pact.bookserviceprovider.controller;

import com.panderalabs.pact.bookserviceprovider.domain.Book;
import com.panderalabs.pact.bookserviceprovider.exception.BadRequestException;
import com.panderalabs.pact.bookserviceprovider.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("books")
public class BookController {

    private BookService bookService;

    public BookController(final BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public Collection<Book> getBooks() {
        return bookService.getBooks();
    }

    @PostMapping
    public ResponseEntity<?> createBook(@Valid @RequestBody Book book) {
        bookService.createBook(book);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{isbn}")
                .buildAndExpand(book.getIsbn()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/{isbn}")
    public Book getBookByIsbn(@PathVariable String isbn) {
        return bookService.getBook(isbn);
    }

    @PutMapping(path = "/{isbn}")
    public Book updateBook(@PathVariable String isbn, @Valid @RequestBody Book book) {
        if (!book.getIsbn().equals(isbn)) {
            throw new BadRequestException("ISBN in Url and body do not match");
        }

        return bookService.updateBook(book);


    }


    @DeleteMapping(path = "/{isbn}")
    public ResponseEntity<?> deleteBook(@PathVariable String isbn) {
        bookService.deleteBook(isbn);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "count")
    public Integer getCount() {
        return bookService.getCount();
    }

}
