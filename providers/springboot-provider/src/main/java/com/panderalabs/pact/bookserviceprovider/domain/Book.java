package com.panderalabs.pact.bookserviceprovider.domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.ISBN;
import javax.validation.constraints.NotNull;
import java.time.Year;

public class Book {
    @NotNull
    private String title;
    @NotNull
    private String author;
    @NotNull
    @ISBN(type = ISBN.Type.ISBN_10)
    private String isbn;

    private Year year;

    //@JsonCreator
    public Book(@JsonProperty("title") final String title, @JsonProperty("author") final String author, @JsonProperty("isbn") final String isbn) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }


    @JsonCreator
    public Book(@JsonProperty("title") final String title, @JsonProperty("author") final String author, @JsonProperty("isbn") final String isbn,
                @JsonProperty("year") final Year year) {
        this(title, author, isbn);
        this.year = year;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(final Year year) {
        this.year = year;
    }
}
