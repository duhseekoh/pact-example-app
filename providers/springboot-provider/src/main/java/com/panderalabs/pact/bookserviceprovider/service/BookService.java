package com.panderalabs.pact.bookserviceprovider.service;

import com.panderalabs.pact.bookserviceprovider.domain.Book;
import com.panderalabs.pact.bookserviceprovider.repository.BookDataStore;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class BookService {

    private BookDataStore bookRepo = BookDataStore.INSTANCE;

    public Collection<Book> getBooks() {
        return this.bookRepo.getBooks();
    }

    public void createBook(Book book) {
        this.bookRepo.addBook(book);
    }

    public Book getBook(String isbn) {
        return this.bookRepo.getBookByIsbn(isbn);
    }

    public Book updateBook(Book book) {
        return this.bookRepo.updateBook(book);
    }

    public void deleteBook(String isbn) {
        this.bookRepo.deleteBook(isbn);
    }

    public int getCount() {
        return this.bookRepo.getBooks().size();
    }
}
