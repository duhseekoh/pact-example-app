package com.panderalabs.pact.bookserviceprovider;

import com.panderalabs.pact.bookserviceprovider.repository.BookDataStore;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@RestController
@Profile("pact-test")
public class StateChangeController {

    @RequestMapping(value = "/pactStateChange", method = RequestMethod.POST)
    public void providerState(@RequestBody Map<String, String> body) {
        String state = body.get("state");

        switch (state) {
            case "Book 0134685997 exists":
            case "Two Books Exist":
                BookDataStore.INSTANCE.initializeData();
                break;
            case "Zero Books Exist":
                BookDataStore.INSTANCE.clear();
                break;
        }
    }
}
