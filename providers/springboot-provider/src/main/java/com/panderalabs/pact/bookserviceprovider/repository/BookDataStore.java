package com.panderalabs.pact.bookserviceprovider.repository;

import com.panderalabs.pact.bookserviceprovider.domain.Book;
import com.panderalabs.pact.bookserviceprovider.exception.ResourceNotFoundException;
import com.google.common.collect.Maps;
import java.time.Year;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class BookDataStore {

    public static final BookDataStore INSTANCE = new BookDataStore();

    private Map<String, Book> initialData() {
        Map<String, Book> data = Maps.newHashMap();
        data.put("0134685997", new Book("Effective Java (3rd Edition)", "Joshua Bloch", "0134685997", Year.of(2018)));
        data.put("1491950358", new Book("Building Microservices: Designing Fine-Grained Systems", "Sam Newman", "1491950358", Year.of(2015)));
        data.put("1942788290", new Book("The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win", "Gene Kim", "1942788290"));
        data.put("1501168363", new Book("The Gunslinger", "Stephen King", "1501168363", Year.of(1982)));
        return data;
    }

    private BookDataStore() {
        booksByIsbn = initialData();
    }

    private Map<String, Book> booksByIsbn;

    public void initializeData() {
        this.booksByIsbn = initialData();
    }
    public void clear() {
        this.booksByIsbn.clear();
    }

    public Book getBookByIsbn(String isbn) {
        return Optional.ofNullable(booksByIsbn.get(isbn)).orElseThrow(() -> new ResourceNotFoundException("Cannot locate book with isbn: " + isbn));
    }

    public Collection<Book> getBooks() {
        return booksByIsbn.values();
    }

    public void addBook(Book book) {
        this.booksByIsbn.put(book.getIsbn(), book);
    }

    public Book updateBook(Book book) {
        // throw exception if book doesn't exist
        this.booksByIsbn.get(book.getIsbn());
        // replace existing with this book
        this.booksByIsbn.remove(book.getIsbn());
        this.addBook(book);
        return book;
    }

    public void deleteBook(String isbn) {
        //throw exception if book doesn't exist
        this.booksByIsbn.get(isbn);

        this.booksByIsbn.remove(isbn);
    }

}
