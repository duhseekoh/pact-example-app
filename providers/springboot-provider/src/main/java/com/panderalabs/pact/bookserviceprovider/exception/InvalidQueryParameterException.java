package com.panderalabs.pact.bookserviceprovider.exception;

public class InvalidQueryParameterException extends RuntimeException {
  public InvalidQueryParameterException(String message, Exception cause) {
    super(message, cause);
  }
}
