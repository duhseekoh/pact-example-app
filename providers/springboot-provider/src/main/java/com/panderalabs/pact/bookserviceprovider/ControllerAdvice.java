package com.panderalabs.pact.bookserviceprovider;

import com.panderalabs.pact.bookserviceprovider.controller.BookController;
import com.panderalabs.pact.bookserviceprovider.exception.BadRequestException;
import com.panderalabs.pact.bookserviceprovider.exception.InvalidQueryParameterException;
import com.panderalabs.pact.bookserviceprovider.exception.QueryParameterRequiredException;
import com.panderalabs.pact.bookserviceprovider.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

@RestControllerAdvice(basePackageClasses = { BookController.class })
public class ControllerAdvice {

    Logger log = LoggerFactory.getLogger(ControllerAdvice.class);

    @ExceptionHandler({ BadRequestException.class })
    @ResponseBody
    public ResponseEntity<String> handleBadRequestException(HttpServletRequest request, Throwable ex) {
        log.error("Bad Request Exception is: {}", ex);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>("{\"error\": \"" + ex.getMessage() + "\"}", headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ResourceNotFoundException.class })
    @ResponseBody
    public ResponseEntity<String> handleResourceNotFoundException(HttpServletRequest request, Throwable ex) {
        log.error("Not Found Exception is: {}", ex);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>("{\"error\": \"" + ex.getMessage() + "\"}", headers, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({ InvalidQueryParameterException.class, QueryParameterRequiredException.class })
    @ResponseBody
    public ResponseEntity<String> handleControllerException(HttpServletRequest request, Throwable ex) {
        log.error("Parameter Exception is: {}", ex);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>("{\"error\": \"" + ex.getMessage() + "\"}", headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ MethodArgumentNotValidException.class, ValidationException.class })
    @ResponseBody
    public ResponseEntity<String> handleValidationException(HttpServletRequest request, Throwable ex) {
        log.error("Validation Exception is: {}", ex);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>("{\"error\": \"" + ex.getMessage() + "\"}", headers, HttpStatus.BAD_REQUEST);
    }
}
