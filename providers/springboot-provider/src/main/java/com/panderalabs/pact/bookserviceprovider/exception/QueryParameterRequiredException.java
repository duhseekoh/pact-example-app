package com.panderalabs.pact.bookserviceprovider.exception;

public class QueryParameterRequiredException extends RuntimeException {
  public QueryParameterRequiredException(String message) {
    super(message);
  }
}
