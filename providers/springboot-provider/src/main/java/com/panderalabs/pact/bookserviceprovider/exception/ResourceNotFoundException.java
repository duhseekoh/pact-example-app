package com.panderalabs.pact.bookserviceprovider.exception;


public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String msg) {
        super(msg);
    }

}
