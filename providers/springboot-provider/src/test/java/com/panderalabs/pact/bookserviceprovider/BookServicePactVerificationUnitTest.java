package com.panderalabs.pact.bookserviceprovider;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.RestPactRunner;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit.loader.PactBrokerAuth;
import au.com.dius.pact.provider.junit.target.TestTarget;
import au.com.dius.pact.provider.spring.target.MockMvcTarget;
import com.panderalabs.pact.bookserviceprovider.controller.BookController;
import com.panderalabs.pact.bookserviceprovider.repository.BookDataStore;
import com.panderalabs.pact.bookserviceprovider.service.BookService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Validates the interface without actually starting up the running server by unit testing the Spring Controller
 */
@RunWith(RestPactRunner.class)
@Provider("BookService")
@PactBroker(
        host = "panderalabs.pact.dius.com.au",
        protocol = "https",
        port = "443",
        authentication = @PactBrokerAuth(username = "XrmMVYjSHZhlIjC1rQaYXD2A1WnI8p4m", password = "k9get5R4iSuGqmK1BAhiJIcpOLPKL")
)
public class BookServicePactVerificationUnitTest {

    @InjectMocks
    private BookService bookService = new BookService();

    @InjectMocks
    private BookController bookController = new BookController(bookService);

    @InjectMocks
    private ControllerAdvice exceptionAdvice = new ControllerAdvice();

    @Autowired
    private BookDataStore bookDataStore;


    @TestTarget
    public MockMvcTarget target = new MockMvcTarget();

    @Before //Method will be run before each test of interaction
    public void before() {
        //initialize your mocks using your mocking framework
        MockitoAnnotations.initMocks(this);

        //configure the MockMvcTarget with your controller and controller advice
        target.setControllers(bookController);
        target.setControllerAdvice(exceptionAdvice);
    }

    @State(value = { "Book 0134685997 exists", "Two Books Exist" })
    public void defaultData() {
        BookDataStore.INSTANCE.initializeData();
    }

    @State(value = "Zero Books Exist")
    public void emptyData() {
        BookDataStore.INSTANCE.clear();
    }
}
