package com.panderalabs.pact.bookserviceprovider;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit.loader.PactBrokerAuth;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import au.com.dius.pact.provider.spring.SpringRestPactRunner;
import com.panderalabs.pact.bookserviceprovider.repository.BookDataStore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/***
 * Verify pact by launching actual spring boot application and routing requests to it
 */
@RunWith(SpringRestPactRunner.class)
@Provider("BookService")
@PactBroker(
        host = "panderalabs.pact.dius.com.au",
        protocol = "https",
        port = "443",
        authentication = @PactBrokerAuth(username = "XrmMVYjSHZhlIjC1rQaYXD2A1WnI8p4m", password = "k9get5R4iSuGqmK1BAhiJIcpOLPKL")
)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("pact-test")
public class BookServicePactVerificationIntegrationTest {

    @TestTarget
    public final Target target = new HttpTarget(8080);

    @State(value = { "Book 0134685997 exists", "Two Books Exist" })
    public void defaultData() {
        BookDataStore.INSTANCE.initializeData();
    }

    @State(value = "Zero Books Exist")
    public void emptyData() {
        BookDataStore.INSTANCE.clear();
    }
}
